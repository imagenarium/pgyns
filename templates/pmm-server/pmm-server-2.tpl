<@requirement.NODE 'pmm-server' 'pmm-server-${namespace}' />

<@requirement.PARAM name='PUBLISHED_PORT' value='3333' type='port' />

<@img.TASK 'pmm-server-${namespace}' 'imagenarium/pmm-server:2.41.0'>
  <@img.PORT PARAMS.PUBLISHED_PORT '80' />

  <@img.ENV 'DISABLE_UPDATES' 'true' />
  <@img.ENV 'ENABLE_BACKUP_MANAGEMENT' 'true' />
  <@img.ENV 'DATA_RETENTION' '24h' />
  <@img.ENV 'ENABLE_ALERTING' 'true' />

  <@img.NODE_REF 'pmm-server' />

  <@img.VOLUME '/srv' />

  <@img.CHECK_PORT '80' />
</@img.TASK>
