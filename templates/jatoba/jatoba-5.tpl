<@requirement.NODE 'jatoba' 'jatoba-${namespace}' />

<@requirement.PARAM name='PUBLISHED_PORT' type='port' required='false' description='Specify postgres external port (for example 5432)' />
<@requirement.PARAM name='POSTGRES_USER' value='postgres' scope='global' />
<@requirement.PARAM name='POSTGRES_PASSWORD' scope='global' />
<@requirement.PARAM name='POSTGRES_DB' value='postgres' />
<@requirement.PARAM name='LICENSE_EMAIL' />
<@requirement.PARAM name='LICENSE_KEY' />
<@requirement.PARAM name='PMM_SERVER_PASSWORD' value='admin' regexp='[0-9a-zA-Z]{5,}' description='Не следует менять при первичном развертывании!' />

<@requirement.PARAM name='CMD' value='-c max_connections=1000 -c shared_buffers=128MB -c work_mem=128MB -c max_worker_processes=8 -c maintenance_work_mem=128MB -c wal_buffers=1MB' type='textarea' />

<@img.TASK 'jatoba-${namespace}' 'imagenarium/jatoba-img:5.4.1' PARAMS.CMD>
  <@img.NODE_REF 'jatoba' />

  <@img.PORT PARAMS.PUBLISHED_PORT '5432' />

  <@img.VOLUME '/var/lib/jatoba/5/data' />
  <@img.VOLUME '/var/lib/jatoba/5' />
  <@img.VOLUME '/license' />

  <@img.BIND '/dev/shm' '/dev/shm' />
  <@img.BIND '/sys/kernel/mm/transparent_hugepage' '/thp' />

  <@img.ENV 'LICENSE_EMAIL' PARAMS.LICENSE_EMAIL />
  <@img.ENV 'LICENSE_KEY' PARAMS.LICENSE_KEY />
  <@img.ENV 'LICENSE_TYPE' 'offline' />
  <@img.ENV 'POSTGRES_USER' PARAMS.POSTGRES_USER />
  <@img.ENV 'POSTGRES_PASSWORD' PARAMS.POSTGRES_PASSWORD />
  <@img.ENV 'POSTGRES_DB' PARAMS.POSTGRES_DB />

  <@img.CHECK_PORT '5432' />
</@img.TASK>

<@img.TASK 'ytb-pmm-client-${namespace}' 'imagenarium/pmm-client:2.41.0'>
  <@img.NODE_REF 'jatoba' />

  <@img.ENV 'PMM_AGENT_SERVER_ADDRESS' 'pmm-server-${namespace}:443' />
  <@img.ENV 'PMM_AGENT_SERVER_PASSWORD' PARAMS.PMM_SERVER_PASSWORD />
  <@img.ENV 'POSTGRES_USER' PARAMS.POSTGRES_USER />
  <@img.ENV 'POSTGRES_PASSWORD' PARAMS.POSTGRES_PASSWORD />
  <@img.ENV 'DB_SERVICE' 'jatoba' />
  <@img.ENV 'QUERY_SOURCE' 'pgstatements' />

  <@img.CHECK_PORT '7777' />
</@img.TASK>
