<@requirement.PARAM name='YENISEI_USER' value='admin' scope='global' />
<@requirement.PARAM name='YENISEI_PASSWORD' scope='global' />
<@requirement.PARAM name='YENISEI_COOKIE' value='ynscookie' scope='global' />

<@requirement.PARAM name='PMM_SERVER_PASSWORD' value='admin' regexp='[0-9a-zA-Z]{5,}' description='Не следует менять при первичном развертывании!' />

<#assign nodes = [] />

<#list 1..2 as index>
  <#if index != NODE_INDEX>
    <#assign nodes += ['yenisei@yns-${index}-${namespace}'] />
  </#if>
</#list>

<@img.TASK 'yns-${NODE_INDEX}-${namespace}' 'imagenarium/yenisei-cluster:2.1'>
  <@img.NODE_REF '${NODE_INDEX}' />

  <@img.VOLUME '/opt/yenisei/data' />
  <@img.VOLUME '/etc/yenisei' />

  <@img.ENV 'NODENAME' 'yns-${NODE_INDEX}-${namespace}' />
  <@img.ENV 'RECV_BUFFER' '262144' />
  <@img.ENV 'YENISEI_USER' PARAMS.YENISEI_USER />
  <@img.ENV 'YENISEI_PASSWORD' PARAMS.YENISEI_PASSWORD />
  <@img.ENV 'YENISEI_COOKIE' PARAMS.YENISEI_COOKIE />
  <@img.ENV 'SEEDLIST' nodes?join(',') />
  <@img.ENV 'PARTITIONS' '8' />
  <@img.ENV 'REPLICAS' '1' />
  <@img.ENV 'ERL_EPMD_PORT' '4369' />
  <@img.ENV 'CLUSTERING_PORT' '9110' />

  <@img.CHECK_PORT '5984' />
</@img.TASK>

<@img.TASK 'yns-pmm-client-${NODE_INDEX}-${namespace}' 'imagenarium/yns-pmm-agent:2.41.0'>
  <@img.NODE_REF '${NODE_INDEX}' />

  <@img.ENV 'PMM_AGENT_SERVER_ADDRESS' 'pmm-server-${namespace}:443' />
  <@img.ENV 'PMM_AGENT_SERVER_PASSWORD' PARAMS.PMM_SERVER_PASSWORD />
  <@img.ENV 'YENISEI_USER' PARAMS.YENISEI_USER />
  <@img.ENV 'YENISEI_PASSWORD' PARAMS.YENISEI_PASSWORD />
  <@img.ENV 'YENISEI_CONTAINER_NAME' 'yns-${NODE_INDEX}-${namespace}' />

  <@img.CHECK_PORT '7777' />
</@img.TASK>
