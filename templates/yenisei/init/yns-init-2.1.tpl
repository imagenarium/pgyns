<@requirement.PARAM name='YENISEI_USER' value='admin' scope='global' />
<@requirement.PARAM name='YENISEI_PASSWORD' type='password' scope='global' />

<@docker.CONTAINER 'yns-checker-${namespace}' 'imagenarium/yenisei-cluster:2.1'>
  <@container.ENTRY '/check_cluster.sh' />
  <@container.ENV 'YENISEI_HOST' 'http://${PARAMS.YENISEI_USER}:${PARAMS.YENISEI_PASSWORD}@yns-1-${namespace}:5984' />
  <@container.EPHEMERAL />
</@docker.CONTAINER>
