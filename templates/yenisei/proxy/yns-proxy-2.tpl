<@requirement.NODE ref='proxy' primary='yns-proxy-${namespace}' single='false' />

<@requirement.PARAM name='PUBLISHED_PORT' required='false' type='port' />

<#assign nodes = [] />

<#list 1..2 as index>
  <#assign nodes += ['http://yns-${index}-${namespace}:5984'] />
</#list>

<@img.TASK 'yns-proxy-${namespace}' 'imagenarium/traefik:2.9.6'>
  <@img.NODE_REF 'proxy' />

  <@img.PORT PARAMS.PUBLISHED_PORT '80' />
  <@img.ENV 'SERVERS' nodes?join(',') />

  <@img.CHECK_PORT '80' />
</@img.TASK>
